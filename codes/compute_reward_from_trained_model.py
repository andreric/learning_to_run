import sys
from osim.env.run import RunEnv
from build_model import build_model
import numpy as np
from joblib import Parallel, delayed

def compute_performance_model(xmodels, model_filename, batch_size, seed):

    env = RunEnv(visualize=False)

    observation_space_size = env.observation_space.shape
    nb_actions = env.action_space.shape[0]
    nb_env_outputs = env.noutput

    #print('Trained model: %s'%(filename))
    #print('N steps trained: %s'%(n_steps))
    #xmodels = (n_steps,)

    agents = list()
    for m in xmodels:
        agent = build_model('test', 'trained_models/%s_steps_%d'%(model_filename,m),\
			    batch_size, nb_actions, observation_space_size, nb_env_outputs)
        agents.append(agent)
 

    def my_controller(observation):
        size = agents[0].forward(observation).shape
        actions = np.zeros((size[0],len(agents)))
        for i,ag in enumerate(agents):
            actions[:,i] = ag.forward(observation)
        return actions.mean(axis=1)
#        return np.average(actions,axis=1,weights=weights)

    difficulty = 2
    total_reward = 0.0
    observation = env.reset(difficulty=difficulty,seed=seed)

    for i in range(1000):
        observation, reward, done, info = env.step(my_controller(observation))
        total_reward += reward
        if done:
            env.reset()
            break
    #fh.write('%d;%f\n'%(n_steps,total_reward))
    #fh.flush()
    print("Total reward %f" %(total_reward))
    return total_reward


#fh = open('scores_steps.txt','w')
#models = range(20000,700000,10000)
models = (320000,340000,370000)

# 594 no crowdai
#models = (180000,310000,320000,330000,340000,350000,360000,370000,590000,660000,670000)

#models = (100000,120000,130000,160000,170000,260000,270000)
models = range(310000,380000,10000) 
#weights = (0.9,0.74,1.24,0.95,1,0.98,0.82,1.10,0.87,0.89,0.92)
#weights = np.array(weights)
#weights = weights/weights.sum()
#models = range(10000,700000,10000)
#models = (670000,)
batch_size = 32
model_filename = 'DDPG-003'
seeds = (0,50,100)
rewards = np.zeros((3,1))
for i,s in enumerate(seeds):
    rewards[i] = compute_performance_model(models, model_filename, batch_size, s)
print(rewards)
print(rewards.mean())
#Parallel(n_jobs=4)(delayed(compute_performance_model)(m) for m in models)
#fh.close()

#obs_arr = np.array(obs)

#plt.boxplot(obs_arr)
#plt.title('Difficulty %d'%(difficulty))
#plt.show()


