# -*- coding: utf-8 -*-
"""
Created on Thu Jul  6 09:42:14 2017

@author: andrerg
"""

#from lightCNN import build_model
from keras.models import Model
from keras.layers import Dense, Activation, Flatten, Input, Dropout
from keras.layers.merge import concatenate
from keras.optimizers import Adam
from keras.initializers import RandomUniform
from keras.layers.normalization import BatchNormalization
#from keras.callbacks import EarlyStopping, ModelCheckpoint

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

from rl.agents import DDPGAgent, ContinuousDQNAgent
from rl.memory import SequentialMemory
from rl.random import OrnsteinUhlenbeckProcess

# from keras.optimizers import RMSprop


def build_rl_model(model_name, params):
    '''
    Implements several Reinforcement Learning models.
    '''
#    batch_size = params['batch_size']
    nb_actions = params['nb_actions']
    observation_space_size = params['observation_space_size']
    nb_env_outputs = params['nb_env_outputs']

    if model_name.lower().startswith('ddpg-001'):
        batch_size = 32
        agent = rl_model_ddpg_sh01(nb_actions, batch_size,\
                              observation_space_size, nb_env_outputs,\
                              batch_norm=False, dropout=0, opt_metric='mae')

    elif model_name.lower().startswith('ddpg-004'):
        batch_size = 64
        agent = rl_model_ddpg_sh01(nb_actions, batch_size, \
                              observation_space_size, nb_env_outputs, \
                              batch_norm=False, dropout=0, opt_metric='mae')

    elif model_name.lower().startswith('ddpg-005'):
        batch_size = 32
        agent = rl_model_ddpg_sh01(nb_actions, batch_size, \
                              observation_space_size, nb_env_outputs, \
                              batch_norm=False, dropout=0, opt_metric='mse')

    elif model_name.lower().startswith('ddpg-006'):
        batch_size = 32
        agent = rl_model_ddpg_sh01(nb_actions, batch_size, \
                              observation_space_size, nb_env_outputs, \
                              batch_norm=True, dropout=0, opt_metric='mae')

    elif model_name.lower().startswith('ddpg-007'):
        batch_size=32
        agent = rl_model_ddpg_sh01(nb_actions, batch_size, \
                              observation_space_size, nb_env_outputs, \
                              batch_norm=False, dropout=0.2, opt_metric='mae')

    elif model_name.lower().startswith('ddpg-008'):
        batch_size=32
        agent = rl_model_ddpg_sh01(nb_actions, batch_size, \
                              observation_space_size, nb_env_outputs, \
                              batch_norm=True, dropout=0.2, opt_metric='mae')
                              
    elif model_name.lower().startswith('ddpg-009'):
        batch_size=32
        agent = rl_model_ddpg_dp01(nb_actions, batch_size, \
                              observation_space_size, nb_env_outputs, \
                              batch_norm=False, dropout=0, opt_metric='mae')                              
                              
    elif model_name.lower().startswith('ddpg-010'):
        batch_size=32
        agent = rl_model_ddpg_dp02(nb_actions, batch_size, \
                              observation_space_size, nb_env_outputs, \
                              batch_norm=False, dropout=0, opt_metric='mae')                                
    else:
        raise Exception('Unknown model name: %s'%(model_name))
        
    # start with a good (pre-trained) model or from a random one
    if params['warm_start']:
        agent.load_weights('trained_models/%s'%(model_name))
    
    return agent



def rl_model_ddpg_sh01(nb_actions, batch_size, obs_space_size, \
                              nb_env_outputs, batch_norm, dropout, opt_metric):
    
    final_layer_init = RandomUniform(minval=-3*10e-3, maxval=3*10e-3)
    kernel_init = 'lecun_uniform'
    bias_init = 'lecun_uniform'

    # Create networks for DDPG
    
    # actor model
    actor_input = Input(shape=(1,) + obs_space_size)
    
    ### layer 1
    flattened_actor_observation = Flatten()(actor_input)
    x = Dense(128, kernel_initializer=kernel_init, \
                       bias_initializer=bias_init)(flattened_actor_observation)
    x = Activation('elu')(x)

    ### layer 2        
    x = Dense(64, kernel_initializer=kernel_init, \
                                    bias_initializer=bias_init)(x)
    x = Activation('elu')(x)
    
    ### Output layer        
    x = Dense(nb_actions, kernel_initializer=final_layer_init, \
                                    bias_initializer=final_layer_init)(x)
    x = Activation('sigmoid')(x)
    actor = Model(inputs=actor_input, outputs=x)
    
    # critic model
    action_input = Input(shape=(nb_actions,), name='action_input')
    observation_input = Input(shape=(1,)+obs_space_size, name='observation_input')
    flattened_observation = Flatten()(observation_input)
    x = concatenate([action_input, flattened_observation])
    x = Dense(128, kernel_initializer=kernel_init, \
                                    bias_initializer=bias_init)(x)
    x = Activation('elu')(x)
        
    x = Dense(64, kernel_initializer=kernel_init, \
                                    bias_initializer=bias_init)(x)
    x = Activation('elu')(x)

    x = Dense(1, kernel_initializer=final_layer_init, \
                    bias_initializer=final_layer_init)(x)
    x = Activation('linear')(x)
    critic = Model(inputs=[action_input, observation_input], outputs=x)
    
    # Set up the agent for training
    memory = SequentialMemory(limit=100000, window_length=1)
    random_process = OrnsteinUhlenbeckProcess(theta=.15, mu=0.,\
                                                 sigma=.3, size=nb_env_outputs)
    agent = DDPGAgent(nb_actions=nb_actions, actor=actor, critic=critic,\
                    critic_action_input=action_input, batch_size=batch_size,\
                    memory=memory, nb_steps_warmup_critic=100, \
                    nb_steps_warmup_actor=100,\
                    random_process=random_process, gamma=.99,\
                    target_model_update=1e-3, delta_clip=1)
                    
    #agent = ContinuousDQNAgent(nb_actions=env.noutput, V_model=V_model, L_model=L_model, mu_model=mu_model,
    #                            memory=memory, nb_steps_warmup=1000, random_process=random_process,
    #                            gamma=.99, target_model_update=0.1)
    
    agent.compile(Adam(lr=.001, clipnorm=1.), metrics=[opt_metric])
    
    return agent



def rl_model_ddpg_sh02(nb_actions, batch_size, obs_space_size, \
                              nb_env_outputs, batch_norm, dropout, opt_metric):
    
    final_layer_init = RandomUniform(minval=-3*10e-3, maxval=3*10e-3)
    kernel_init = 'lecun_uniform'
    bias_init = 'lecun_uniform'

    # Create networks for DDPG
    
    # actor model
    actor_input = Input(shape=(1,) + obs_space_size)
    
    ### layer 1
    flattened_actor_observation = Flatten()(actor_input)
    x = Dense(100, kernel_initializer=kernel_init, bias_initializer=bias_init)(flattened_actor_observation)
    x = Activation('relu')(x)

    ### layer 2        
    x = Dense(50, kernel_initializer=kernel_init, bias_initializer=bias_init)(x)
    x = Activation('relu')(x)

    ### layer 3
    x = Dense(25, kernel_initializer=kernel_init, bias_initializer=bias_init)(x)
    x = Activation('relu')(x)

    ### Output layer
    x = Dense(nb_actions, kernel_initializer=final_layer_init, bias_initializer=final_layer_init)(x)
    x = Activation('sigmoid')(x)
    actor = Model(inputs=actor_input, outputs=x)
    
    # critic model
    action_input = Input(shape=(nb_actions,), name='action_input')
    observation_input = Input(shape=(1,)+obs_space_size, name='observation_input')
    flattened_observation = Flatten()(observation_input)
    x = concatenate([action_input, flattened_observation])
    x = Dense(100, kernel_initializer=kernel_init, bias_initializer=bias_init)(x)
    x = Activation('relu')(x)
        
    x = Dense(100, kernel_initializer=kernel_init, bias_initializer=bias_init)(x)
    x = Activation('relu')(x)

    x = Dense(1, kernel_initializer=final_layer_init, bias_initializer=final_layer_init)(x)
    x = Activation('linear')(x)
    critic = Model(inputs=[action_input, observation_input], outputs=x)
    
    # Set up the agent for training
    memory = SequentialMemory(limit=100000, window_length=1)
    random_process = OrnsteinUhlenbeckProcess(theta=.15, mu=0.,\
                                                 sigma=.3, size=nb_env_outputs)
    agent = DDPGAgent(nb_actions=nb_actions, actor=actor, critic=critic,\
                    critic_action_input=action_input, batch_size=batch_size,\
                    memory=memory, nb_steps_warmup_critic=100, \
                    nb_steps_warmup_actor=100,\
                    random_process=random_process, gamma=.99,\
                    target_model_update=1e-3, delta_clip=1)
                    
    #agent = ContinuousDQNAgent(nb_actions=env.noutput, V_model=V_model, L_model=L_model, mu_model=mu_model,
    #                            memory=memory, nb_steps_warmup=1000, random_process=random_process,
    #                            gamma=.99, target_model_update=0.1)
    
    agent.compile(Adam(lr=.001, clipnorm=1.), metrics=[opt_metric])
    
    return agent



def rl_model_ddpg_dp01(nb_actions, batch_size, obs_space_size, \
                              nb_env_outputs, batch_norm, dropout, opt_metric):
    
    final_layer_init = RandomUniform(minval=-3*10e-3, maxval=3*10e-3)
    kernel_init = 'lecun_uniform'
    bias_init = 'lecun_uniform'

    # Create networks for DDPG
    
    # actor model
    actor_input = Input(shape=(1,) + obs_space_size)
    
    ### layer 1
    flattened_actor_observation = Flatten()(actor_input)
    x = Dense(64, kernel_initializer=kernel_init, \
                       bias_initializer=bias_init)(flattened_actor_observation)
    if batch_norm:
        x = BatchNormalization()(x)
    x = Activation('elu')(x)
    if dropout > 0:
        x = Dropout(dropout)(x)

    ### layer 2        
    x = Dense(64, kernel_initializer=kernel_init, \
                                    bias_initializer=bias_init)(x)
    if batch_norm:
        x = BatchNormalization()(x)        
    x = Activation('elu')(x)
    if dropout > 0:
        x = Dropout(dropout)(x)
        
        
    ### layer 3
    x = Dense(64, kernel_initializer=kernel_init, \
                                    bias_initializer=bias_init)(x)
    if batch_norm:
        x = BatchNormalization()(x)        
    x = Activation('elu')(x)
    if dropout > 0:
        x = Dropout(dropout)(x)
        
    ### Output layer        
    x = Dense(nb_actions, kernel_initializer=final_layer_init, \
                                    bias_initializer=final_layer_init)(x)
    x = Activation('sigmoid')(x)
    actor = Model(inputs=actor_input, outputs=x)
    
    # critic model
    action_input = Input(shape=(nb_actions,), name='action_input')
    observation_input = Input(shape=(1,)+obs_space_size, name='observation_input')
    flattened_observation = Flatten()(observation_input)
    x = concatenate([action_input, flattened_observation])
    x = Dense(128, kernel_initializer=kernel_init, \
                                    bias_initializer=bias_init)(x)
    if batch_norm:
        x = BatchNormalization()(x)
    x = Activation('elu')(x)
    if dropout > 0:
        x = Dropout(dropout)(x)
        
    x = Dense(64, kernel_initializer=kernel_init, \
                                    bias_initializer=bias_init)(x)
    if batch_norm:
        x = BatchNormalization()(x)        
    x = Activation('elu')(x)
    if dropout > 0:
        x = Dropout(dropout)(x)

    x = Dense(1, kernel_initializer=final_layer_init, \
                    bias_initializer=final_layer_init)(x)
    x = Activation('linear')(x)
    critic = Model(inputs=[action_input, observation_input], outputs=x)
    
    # Set up the agent for training
    memory = SequentialMemory(limit=100000, window_length=1)
    random_process = OrnsteinUhlenbeckProcess(theta=.15, mu=0.,\
                                                 sigma=.3, size=nb_env_outputs)
    agent = DDPGAgent(nb_actions=nb_actions, actor=actor, critic=critic,\
                    critic_action_input=action_input, batch_size=batch_size,\
                    memory=memory, nb_steps_warmup_critic=100, \
                    nb_steps_warmup_actor=100,\
                    random_process=random_process, gamma=.99,\
                    target_model_update=1e-3, delta_clip=1)
                    
    #agent = ContinuousDQNAgent(nb_actions=env.noutput, V_model=V_model, L_model=L_model, mu_model=mu_model,
    #                            memory=memory, nb_steps_warmup=1000, random_process=random_process,
    #                            gamma=.99, target_model_update=0.1)
    
    agent.compile(Adam(lr=.001, clipnorm=1.), metrics=[opt_metric])    
    
    return agent
    
    


def rl_model_ddpg_dp02(nb_actions, batch_size, obs_space_size, \
                              nb_env_outputs, batch_norm, dropout, opt_metric):
    
    final_layer_init = RandomUniform(minval=-3*10e-3, maxval=3*10e-3)
    kernel_init = 'lecun_uniform'
    bias_init = 'lecun_uniform'

    # Create networks for DDPG
    
    # actor model
    actor_input = Input(shape=(1,) + obs_space_size)
    
    ### layer 1
    flattened_actor_observation = Flatten()(actor_input)
    x = Dense(64, kernel_initializer=kernel_init, \
                       bias_initializer=bias_init)(flattened_actor_observation)
    if batch_norm:
        x = BatchNormalization()(x)
    x = Activation('elu')(x)
    if dropout > 0:
        x = Dropout(dropout)(x)

    ### layer 2        
    x = Dense(64, kernel_initializer=kernel_init, \
                                    bias_initializer=bias_init)(x)
    if batch_norm:
        x = BatchNormalization()(x)        
    x = Activation('elu')(x)
    if dropout > 0:
        x = Dropout(dropout)(x)
        
        
    ### layer 3
    x = Dense(64, kernel_initializer=kernel_init, \
                                    bias_initializer=bias_init)(x)
    if batch_norm:
        x = BatchNormalization()(x)        
    x = Activation('elu')(x)
    if dropout > 0:
        x = Dropout(dropout)(x)
        
    ### Output layer        
    x = Dense(nb_actions, kernel_initializer=final_layer_init, \
                                    bias_initializer=final_layer_init)(x)
    x = Activation('sigmoid')(x)
    actor = Model(inputs=actor_input, outputs=x)
    
    # critic model
    action_input = Input(shape=(nb_actions,), name='action_input')
    observation_input = Input(shape=(1,)+obs_space_size, name='observation_input')
    flattened_observation = Flatten()(observation_input)
    x = concatenate([action_input, flattened_observation])
    x = Dense(128, kernel_initializer=kernel_init, \
                                    bias_initializer=bias_init)(x)
    if batch_norm:
        x = BatchNormalization()(x)
    x = Activation('elu')(x)
    if dropout > 0:
        x = Dropout(dropout)(x)
        
    x = Dense(64, kernel_initializer=kernel_init, \
                                    bias_initializer=bias_init)(x)
    if batch_norm:
        x = BatchNormalization()(x)        
    x = Activation('elu')(x)
    if dropout > 0:
        x = Dropout(dropout)(x)

    x = Dense(1, kernel_initializer=final_layer_init, \
                    bias_initializer=final_layer_init)(x)
    x = Activation('linear')(x)
    critic = Model(inputs=[action_input, observation_input], outputs=x)
    
    # Set up the agent for training
    memory = SequentialMemory(limit=1000000, window_length=1)
    random_process = OrnsteinUhlenbeckProcess(theta=.15, mu=0.,\
                                                 sigma=.3, size=nb_env_outputs)
    agent = DDPGAgent(nb_actions=nb_actions, actor=actor, critic=critic,\
                    critic_action_input=action_input, batch_size=batch_size,\
                    memory=memory, nb_steps_warmup_critic=1000, \
                    nb_steps_warmup_actor=1000,\
                    random_process=random_process, gamma=.99,\
                    target_model_update=1e-3, delta_clip=1)
                    
    #agent = ContinuousDQNAgent(nb_actions=env.noutput, V_model=V_model, L_model=L_model, mu_model=mu_model,
    #                            memory=memory, nb_steps_warmup=1000, random_process=random_process,
    #                            gamma=.99, target_model_update=0.1)
    
    agent.compile(Adam(lr=.001, clipnorm=1.), metrics=[opt_metric])    
    
    return agent
    