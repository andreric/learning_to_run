# Derived from keras-rl
import opensim as osim
import numpy as np

#from lightCNN import build_model
from build_model import build_rl_model
from rl.callbacks import ModelIntervalCheckpoint
from osim.env import *

import argparse
#import math

# Command line parameters
parser = argparse.ArgumentParser(description='Train or test neural net motor controller')
#parser.add_argument('--train', dest='train', action='store_true', default=True)
#parser.add_argument('--test', dest='train', action='store_false', default=True)
parser.add_argument('--steps', dest='steps', action='store', default=10000)
parser.add_argument('--visualize', dest='visualize', action='store_true', default=False)
parser.add_argument('--model', dest='model', action='store')
parser.add_argument('--warm_start', dest='warm_start', default='False')
args = parser.parse_args()

# Load walking environment
env = RunEnv(args.visualize)
env.reset(difficulty=2)

nb_actions = env.action_space.shape[0]

# Total number of steps in training
nallsteps = int(args.steps)
warm_start = True if args.warm_start.lower() == 'true' else False

model_name = args.model
print('Model name: %s'%(model_name))
print('Warm start: %s'%(warm_start))

params = {'observation_space_size': env.observation_space.shape,
          'nb_actions': nb_actions,
          'nb_env_outputs': env.noutput,
          'warm_start': warm_start
          }

agent = build_rl_model(model_name, params)

model_chkpt = ModelIntervalCheckpoint('trained_models/%s'%model_name, \
                                                interval=10000, verbose=1)
callbacks_list = [model_chkpt]

# Okay, now it's time to learn something! We visualize the training here for show, but this
# slows down training quite a lot. You can always safely abort the training prematurely using
# Ctrl + C.
#if args.train:
agent.fit(env, nb_steps=nallsteps, callbacks = callbacks_list, \
            visualize=False, verbose=1, \
            nb_max_episode_steps=env.timestep_limit, log_interval=10000)
# After training is done, we save the final weights.
agent.save_weights(args.model+'_final', overwrite=True)


#if not args.train:
#    agent.load_weights(args.model)
#     Finally, evaluate our algorithm for 1 episode.
#    agent.test(env, nb_episodes=1, visualize=False, nb_max_episode_steps=500)
