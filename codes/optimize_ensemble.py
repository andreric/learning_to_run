# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import numpy as np
import scipy.optimize as spot

#import sys
from osim.env.run import RunEnv
from build_model import build_rl_model
#import numpy as np

#def func(x,*args):
#    return np.square(x-x.mean()).sum()
#
## Equality constraint: Sum_x == 1 (it's a convex combination)
#cons = ({'type':'ineq','fun': lambda x: x.sum()-1},\
#         {'type':'ineq','fun': lambda x: 1-x.sum()})
#
#args = ()
#x0 = np.random.rand(10,1)
#minimizer_kwargs = {"method":"COBYLA","args":args,"constraints":cons}
#ret = spot.basinhopping(func,x0,minimizer_kwargs=minimizer_kwargs)
#print(ret['x'].sum())



def cost_function(weights,*args):
    '''
    '''
    models = args[0]
    batch_size = args[1]
    model_filename = args[2]
    
    seeds = (0,50,100)
    rewards = np.zeros((3,1))
    for i,s in enumerate(seeds):
#        rewards[i] = np.random.rand()
        rewards[i] = compute_performance_model(weights, models,\
                                           model_filename, batch_size, s)
    return 1.0/(1+rewards.sum())






def compute_performance_model(weights, xmodels, model_filename, batch_size, seed):
    '''
    '''
    env = RunEnv(visualize=False)

    observation_space_size = env.observation_space.shape
    nb_actions = env.action_space.shape[0]
    nb_env_outputs = env.noutput

    params = dict()
    params['nb_actions'] = nb_actions
    params['observation_space_size'] = observation_space_size
    params['nb_env_outputs'] = nb_env_outputs
    params['warm_start'] = True # load pre-trained model
    
    agents = list()
    for m in xmodels:
        agent = build_rl_model(model_filename,params)
        agents.append(agent)
 

    def my_controller(observation, weights):
        size = agents[0].forward(observation).shape
        actions = np.zeros((size[0],len(agents)))
        for i,ag in enumerate(agents):
            actions[:,i] = ag.forward(observation)
#        print(actions.mean(axis=1))
#        print(np.average(actions,axis=1,weights=weights))
#        return actions.mean(axis=1)
        return np.average(actions,axis=1,weights=weights)

    difficulty = 2
    total_reward = 0.0
    observation = env.reset(difficulty=difficulty,seed=seed)

    for i in range(1000):
        observation, reward, done, info = env.step( \
                                        my_controller(observation,weights))
        total_reward += reward
        if done:
            env.reset()
            break
    print("Total reward %f" %(total_reward))
    return total_reward


#fh = open('scores_steps.txt','w')
#models = range(20000,700000,10000)
#models = (320000,340000,370000)

# 594 no crowdai
models = (180000,310000,320000,330000,340000,350000,360000,370000,590000,660000,670000)

#models = (100000,120000,130000,160000,170000,260000,270000)
#models = range(310000,380000,10000) 

batch_size = 32
model_filename = 'DDPG-003'

## Equality constraint: Sum_x == 1 (it's a convex combination)
cons = ({'type':'ineq','fun': lambda x: x.sum()-1},\
        {'type':'ineq','fun': lambda x: 1-x.sum()})

args = (models,batch_size,model_filename)
x0 = np.ones((len(models),))/float(len(models))
minimizer_kwargs = dict(method='COBYLA',args=args,constraints=cons)
ret = spot.basinhopping(cost_function, x0, niter=100,disp=True,\
                        minimizer_kwargs=minimizer_kwargs)
print(ret['x'])
print(ret['lowest_optimization_result'])

np.save('best_weights.npy',ret['x'])
