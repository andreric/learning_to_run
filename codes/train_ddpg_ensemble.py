#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 22 00:44:25 2017

@author: argoncalves
"""

# Derived from keras-rl
import opensim as osim
import numpy as np

#from lightCNN import build_model
from build_model import build_rl_model
from rl.callbacks import ModelIntervalCheckpoint
from osim.env import *

import argparse
from ddpg_ensemble import DDPGEnsembleAgent

from keras.models import Model
from keras.layers import Dense, Activation, Flatten, Input, Dropout
from keras.optimizers import Adam
from keras.initializers import RandomUniform
from keras.layers.merge import concatenate
from keras.layers.normalization import BatchNormalization

from rl.memory import SequentialMemory
from rl.random import OrnsteinUhlenbeckProcess



def create_fuser_agent(components, params):

    final_layer_init = RandomUniform(minval=-3*10e-3, maxval=3*10e-3)
    kernel_init = 'lecun_uniform'
    bias_init = 'lecun_uniform'

    # Create networks for DDPG
    
    # actor model
    actor_input = Input(shape=(1,) + params['observation_space_size'])
    
    ### layer 1
    flattened_actor_observation = Flatten()(actor_input)
    x = Dense(128, kernel_initializer=kernel_init, \
                       bias_initializer=bias_init)(flattened_actor_observation)
    if params['batch_norm']:
        x = BatchNormalization()(x)
    x = Activation('elu')(x)
    if params['dropout'] > 0:
        x = Dropout(params['dropout'])(x)

    ### layer 2        
    x = Dense(64, kernel_initializer=kernel_init, \
                                    bias_initializer=bias_init)(x)
    if params['batch_norm']:
        x = BatchNormalization()(x)        
    x = Activation('elu')(x)
    if params['dropout'] > 0:
        x = Dropout(params['dropout'])(x)
        
    ### Output layer        
    x = Dense(nb_actions, kernel_initializer=final_layer_init, \
                                    bias_initializer=final_layer_init)(x)
    x = Activation('sigmoid')(x)
    actor = Model(inputs=actor_input, outputs=x)
    
    actor_ensemble = {'fuser':actor, 'components':components}
    
    
    
    # critic model
    action_input = Input(shape=(nb_actions,), name='action_input')
    observation_input = Input(shape=(1,)+params['observation_space_size'],\
                                              name='observation_input')
    flattened_observation = Flatten()(observation_input)
    x = concatenate([action_input, flattened_observation])
    x = Dense(128, kernel_initializer=kernel_init, \
                                    bias_initializer=bias_init)(x)
    if params['batch_norm']:
        x = BatchNormalization()(x)
    x = Activation('elu')(x)
    if params['dropout'] > 0:
        x = Dropout(params['dropout'])(x)
        
    x = Dense(64, kernel_initializer=kernel_init, \
                                    bias_initializer=bias_init)(x)
    if params['batch_norm']:
        x = BatchNormalization()(x)        
    x = Activation('elu')(x)
    if params['dropout'] > 0:
        x = Dropout(params['dropout'])(x)

    x = Dense(1, kernel_initializer=final_layer_init, \
                    bias_initializer=final_layer_init)(x)
    x = Activation('linear')(x)
    critic = Model(inputs=[action_input, observation_input], outputs=x)
    
    # Set up the agent for training
    memory = SequentialMemory(limit=100000, window_length=1)
    random_process = OrnsteinUhlenbeckProcess(theta=.15, mu=0.,\
                                                 sigma=.3, size=params['nb_env_outputs'])
    agent = DDPGEnsembleAgent(nb_actions=nb_actions, actor_ensemble=actor_ensemble, critic=critic,\
                    critic_action_input=action_input, batch_size=params['batch_size'],\
                    memory=memory, nb_steps_warmup_critic=100, \
                    nb_steps_warmup_actor=100,\
                    random_process=random_process, gamma=.99,\
                    target_model_update=1e-3, delta_clip=1)
        
    agent.compile(Adam(lr=.001, clipnorm=1.), metrics=[params['opt_metric']])   

    return agent



# Command line parameters
parser = argparse.ArgumentParser(description='Train neural net ensemble')
parser.add_argument('--steps', dest='steps', action='store', default=10000)
parser.add_argument('--visualize', dest='visualize', action='store_true', default=False)
parser.add_argument('--model', dest='model', action='store')
args = parser.parse_args()

# Load walking environment
env = RunEnv(args.visualize)
env.reset(difficulty=2)

nb_actions = env.action_space.shape[0]

# Total number of steps in training
nallsteps = int(args.steps)
#warm_start = True if args.warm_start.lower() == 'true' else False

model_name = args.model
print('Model name: %s'%(model_name))
#print('Warm start: %s'%(warm_start))

params = {'observation_space_size': env.observation_space.shape,
          'nb_actions': nb_actions,
          'nb_env_outputs': env.noutput,
          'warm_start': False
         }


# Create networks for DDPG
comp_models = (180000, 310000, 320000, 330000, 340000, 350000,\
                               360000, 370000, 590000, 660000, 670000)

batch_size = 32
model_filename = 'DDPG-003'

print('Loading components ...')
components = list()
for m in comp_models:
    ag = build_rl_model(model_filename, params)    
    ag.load_weights('trained_models/%s_steps_%d'%(model_filename,m))
    components.append(ag)

###############################################################################

nb_ensemble_inputs = len(comp_models) * nb_actions

params = {'observation_space_size': (nb_ensemble_inputs,),
          'nb_actions': nb_actions,
          'nb_env_outputs': env.noutput,
          'batch_size': batch_size,
          'warm_start': False,
          'batch_norm': False,
          'dropout': 0,
          'opt_metric': 'mse'
         }

agent = create_fuser_agent(components, params) 

callbacks_list = [ModelIntervalCheckpoint('trained_models/%s'%model_name, \
                                                interval=10000, verbose=1)]

# Okay, now it's time to learn something! We visualize the training here for show, but this
# slows down training quite a lot. You can always safely abort the training prematurely using
agent.fit(env, nb_steps=nallsteps, callbacks = callbacks_list, \
            visualize=False, verbose=1, \
            nb_max_episode_steps=env.timestep_limit, log_interval=10000)

# After training is done, we save the final weights.
agent.save_weights(args.model+'_final', overwrite=True)

