import opensim as osim
from osim.http.client import Client
from osim.env import *
import numpy as np
import argparse
from build_model import build_rl_model


def my_controller(observation):
    size = agents[0].forward(observation).shape
    actions = np.zeros((size[0], len(agents)))
    for i, ag in enumerate(agents):
        actions[:, i] = ag.forward(observation)
    return np.median(actions,axis=1).tolist()


# Settings
remote_base = 'http://grader.crowdai.org:1729'

# Command line parameters
PARSER = argparse.ArgumentParser(description='Submit the result to crowdAI')
PARSER.add_argument('--token', dest='token', action='store', required=True)
args = PARSER.parse_args()

env = RunEnv(visualize=False)
client = Client(remote_base)

nb_actions = env.action_space.shape[0]
nb_env_outputs = env.noutput

model_filename = 'DDPG-003'
batch_size = 32

models = (180000,310000,320000,330000,340000,350000,360000,370000,590000,660000,670000) # 594.572181
#models = (320000,330000,340000,350000,370000)  # 433.345417
#models = (100000,120000,130000,160000,170000,260000,270000)  # 577.755932

params = {'observation_space_size': env.observation_space.shape,
          'nb_actions':  env.action_space.shape[0],
          'nb_env_outputs': env.noutput,
          'batch_size': batch_size,
          'warm_start': True
         }


agents = list()
for m in models:
    agent = build_rl_model('%s_steps_%d'%(model_filename, m), params)
    agents.append(agent)

# Create environment
observation = client.env_create(args.token)

# Run a single step
#
# The grader runs 3 simulations of at most 1000 steps each. We stop after the last one
while True:
#    v = np.array(observation).reshape((-1,1,env.observation_space.shape[0]))
    [observation, reward, done, info] = client.env_step(my_controller(observation))
    #print(observation)
    if done:
        observation = client.env_reset()
        if not observation:
            break

client.submit()



# Run a single step
#
# The grader runs 3 simulations of at most 1000 steps each. We stop after the last one
#while True:
#    #v = np.array(observation).reshape((-1,1,env.observation_space.shape[0]))
#    [observation, reward, done, info] = client.env_step(my_controller(observation))
#    print(observation)
#    if done:
#        observation = client.env_reset()
#        if not observation:
#            break
#
#client.submit()
